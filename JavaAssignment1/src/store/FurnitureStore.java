package store;

import chairs.UseOfChairs;

public class FurnitureStore {

	public static void main(String args[]) {

		UseOfChairs useOfChairs = new UseOfChairs();
		useOfChairs.OfficeChair("PLASTIC");
		useOfChairs.OfficeChair("WOODEN");
		useOfChairs.HomeChair("STAINLESS STEEL");

	}
}
package chairs;

public class UseOfChairs {

	public void OfficeChair(String Chair) {

		TypesOfChairs typesOfChairs = new TypesOfChairs();

		if (Chair == "PLASTIC") {
			typesOfChairs.PlasticChair("Artificial", "Wheeled");
		} else if (Chair == "STAINLESS STEEL") {
			typesOfChairs.StainlessSteelChair("Artificial", "Wheeled");
		} else {
			typesOfChairs.WoodenChair("Artificial", "Wheeled");
		}
	}

	public void HomeChair(String Chair) {

		TypesOfChairs typesOfChairs = new TypesOfChairs();

		if (Chair == "PLASTIC") {
			typesOfChairs.PlasticChair("Jute(Natural)", "Rubber");
		} else if (Chair == "STAINLESS STEEL") {
			typesOfChairs.StainlessSteelChair("Jute(Natural)", "Rubber");
		} else {
			typesOfChairs.WoodenChair("Jute(Natural)", "Rubber");
		}
	}

}

package chairs;

public class TypesOfChairs {

	public void WoodenChair(String typeOfFoam, String typeOfLeg) {
		System.out.print("You have purchased a wooden chair of " + typeOfFoam + "foam and " + typeOfLeg + "leg\n");
	}

	public void PlasticChair(String typeOfFoam, String typeOfLeg) {
		System.out.print("You have purchased a plastic chair of " + typeOfFoam + "foam and " + typeOfLeg + "leg\n");
	}

	public void StainlessSteelChair(String typeOfFoam, String typeOfLeg) {
		System.out.print(
				"You have purchased a stainless steel chair of " + typeOfFoam + "foam and " + typeOfLeg + "leg\n");
	}

}
